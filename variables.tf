## AWS Managed AD
variable "mad_admin_username" {
  type    = string
  default = "Administrator"
}

variable "mad_admin_psw_length" {
  type    = number
  default = 15
}

variable "mad_domain_name" {
  type    = string
  default = "mad.sample.com"
}

variable "mad_edition" {
  type    = string
  default = "Standard"
}

variable "mad_type" {
  type    = string
  default = "MicrosoftAD"
}

variable "ad_name" {
  type  = string
  default = "fsx_ad"
}

variable "ad_password" {
  type = string
  default = "__city12west$$"
 }

variable "vpc_id" {
  type = string
  description = "VPC ID for the Amazon FSx for Windows"
}

variable "subnet_ids" {
type =  list(string)
description = "subnet ids for subnets"
}

variable "environment" {
  type = string
  description = "environment type"
}

variable "sg_id" {
  type = string
  description = "security group id"
}

variable "ssm_document_name" {
  type = string
 description = "ssm document name"
}

variable "role_name" {
  type = string
description = "aws iam role"
}

variable "instance_profile_name" {
  type = string
description = "instance profile"
}

variable "key_name" {
  type = string
description = "key name"
}

variable "ami_id" {
  type = string
description = "ami instance id"
}

variable "instance_type" {
  type = string
description = "instance type"
}
