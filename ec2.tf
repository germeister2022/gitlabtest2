resource "aws_iam_instance_profile" "fsx_instance_profile" {
  role = aws_iam_role.fsx_role.name
}

resource "aws_instance" "fsx_instance" {
  count                  = 2
  ami                    = var.ami_id
  instance_type          = var.instance_type
  iam_instance_profile   = aws_iam_instance_profile.fsx_instance_profile.name
  key_name               = var.key_name

  user_data = <<-EOF
    <powershell>
    $secpasswd = ConvertTo-SecureString "${var.ad_password}" -AsPlainText -Force
    $domaincreds = New-Object System.Management.Automation.PSCredential ("Admin", $secpasswd)
    New-SmbGlobalMapping -RemotePath "\\${aws_fsx_windows_file_system.windows_file_system.dns_name}\share" -Credential $domaincreds -LocalPath Z:
    </powershell>
    <persist>true</persist>
  EOF
}
