resource "aws_fsx_windows_file_system" "windows_file_system" {
  depends_on           = [aws_directory_service_directory.fsx_ad]
  deployment_type      = "MULTI_AZ_1"
  storage_type         = "SSD"
  storage_capacity     = 32
  throughput_capacity  = 32
  active_directory_id  = aws_directory_service_directory.fsx_ad.id

  security_group_ids   = [var.sg_id]
  subnet_ids           = var.subnet_ids
  preferred_subnet_id  = var.subnet_ids[0]

  # Tags
  tags = {
    Project     = "Test"
    Environment = "${var.environment}"
  }
}